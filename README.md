# CS:GO 'mod' collection

### Ghost hunting

Scoped weapons and SMG's (and the Negev) are _NOT_ allowed

```
sv_cheats 1;
mp_limitteams 0;
mp_autoteambalance 0;
```
The 'ghost' player needs to execute the following to go invisible:

```
ent_fire !self addoutput "rendermode 10";
```

### Low gravity deathmatch

Recommended workshop map: [Orion](https://steamcommunity.com/sharedfiles/filedetails/?id=2238607969)

Change game mode to Deathmatch:
```
game_mode 2; game_type 1;
```

Load/Reload Orion (or map):
```
changelevel workshop/2238607969/cs_orion;
```

Set other parameters:
```
sv_cheats 1;
mp_limitteams 0;
mp_autoteambalance 0;
mp_teammates_are_enemies 1;
mp_round_restart_delay 0;
weapon_accuracy_nospread 1;
```
If you want you can play around with gravity:
```
sv_gravity 200;
```

### Headshot only

```
mp_damage_headshot_only 1;
```

### Vampire mode

```
mp_damage_vampiric_amount 1;
```

### Respawn in Competitive

```
mp_respawn_on_death_t
mp_respawn_on_death_ct
```

## Untested commands

### xHP mode

Every player needs to execute the ent_fire line

```
sv_cheats 1;
ent_fire !self addoutput "health x";
```
